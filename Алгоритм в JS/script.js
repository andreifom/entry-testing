function test() {
    var n = Number(document.getElementById('n').value); // Сколько элементов хотим получить
    var fibonacci = [0, 1]; // Первые два элемента последовательности Фибоначчи

    for (i = 2; i < n; i ++) {
    // Получаем i-й элемент последовательности как сумму предыдущих двух
    fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
}
    console.log(fibonacci.slice(0,n));
}